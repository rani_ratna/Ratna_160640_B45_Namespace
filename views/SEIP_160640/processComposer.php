<?php
require_once("../../vendor/autoload.php");

if(empty($_POST['studentId']))
{

    echo "I am a Person <br>";
    $obj= new \App\Person();
    $obj->setName($_POST['userName']);
    $obj->setDateOfBirth($_POST['dateOfBirth']);

    echo $obj->getName();
    echo "<br>";
    echo $obj->getDateOfBirth();
}

else{
    echo "I am a student<br>";
    $obj=new \Tap\Student();
    $obj->setName($_POST['userName']);
    $obj->setDateOfBirth($_POST['dateOfBirth']);
    $obj->setStudentId($_POST['studentId']);

    echo $obj->getName();
    echo "<br>";
    echo $obj->getStudentId();
    echo "<br>";
    echo $obj->getDateOfBirth();
}